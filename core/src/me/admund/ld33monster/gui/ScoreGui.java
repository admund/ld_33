package me.admund.ld33monster.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.gui.GuiUtils;
import me.admund.ld33monster.AssetsList;
import me.admund.ld33monster.Logic;

/**
 * Created by admund on 2015-08-22.
 */
public class ScoreGui extends Actor {
    private boolean DEBUG = false;
    private float screenWidth = Gdx.graphics.getWidth();
    private float screenHeight = Gdx.graphics.getHeight();

    private String FPS = "FPS ";
    private String HI = "Hi! ";
    private String ENEMIES_LEFT = "Babies left: ";
    private String TIME = "Time: ";
    private String CONGRATS = "Congratulation ";
    private String YOUR_TIME = "!\nYour time is: ";
    private String TYPE_R = "\nType \"R\" to try again!";
    private String ZNAK = ":";

    private StringBuilder builder = new StringBuilder();

    private long startTime = 0;
    private String winTime = null;

    private ProgressBar shoutBar = null;

    public ScoreGui(Stage guiStage) {
        createProgressBar(guiStage);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        drawDebugGUI(batch);
    }

    public void drawDebugGUI(Batch batch) {
        if(startTime == 0) {
            startTime = System.currentTimeMillis();
        }

        shoutBar.setValue(Logic.player.getShoutPercent());

        BitmapFont font = GameUtils.assetsManager.getBitmapFont(AssetsList.basicFont);
        font.setColor(Color.WHITE);

        if(DEBUG) {
            builder.setLength(0);
            builder.append(FPS);
            builder.append(Gdx.graphics.getFramesPerSecond());
            font.draw(batch, builder.toString(), 0, 20);
        }

        builder.setLength(0);
        builder.append(HI);
        builder.append(Logic.playerName);
        font.draw(batch, builder.toString(), 40, screenHeight - 20);

        int enemiesLeft = Logic.enemiesList.getEnemies();
        builder.setLength(0);
        builder.append(ENEMIES_LEFT);
        builder.append(enemiesLeft);
        font.draw(batch, builder.toString(), 40, screenHeight - 50);

        builder.setLength(0);
        builder.append(TIME);
        builder.append(getTime());
        font.draw(batch, builder.toString(), 40, screenHeight - 80);

        if(enemiesLeft == 0) {
            if(winTime == null) {
                winTime = getTime();
                if(Logic.achievementsProvider != null) {
                    Logic.achievementsProvider.submitScore(getScore(), Logic.playerName);
                }
            }
            builder.setLength(0);
            builder.append(CONGRATS);
            builder.append(Logic.playerName);
            builder.append(YOUR_TIME);
            builder.append(winTime);
            builder.append(TYPE_R);
            font.draw(batch, builder.toString(), screenWidth * .4f, screenHeight * .8f);
        }
    }

    protected ProgressBar.ProgressBarStyle createProgressBarStyle() {
        ProgressBar.ProgressBarStyle style = new ProgressBar.ProgressBarStyle();
        style.background = GuiUtils.createSpriteDrawable("loading.atlas", "progressbar_back");
        //style.knob = GuiUtils.createSpriteDrawable("loading.atlas", "knob");
        style.knobBefore = GuiUtils.createSpriteDrawable("loading.atlas", "progressbar_front");
        return style;
    }

    private void createProgressBar(Stage guiStage) {
        shoutBar = new ProgressBar(0, 1, 0.1f, false, createProgressBarStyle());
        shoutBar.setSize(200, 30);
        shoutBar.setPosition(screenWidth - 300, screenHeight - 50);
        guiStage.addActor(shoutBar);
    }

    private int getScore() {
        return (int)(System.currentTimeMillis() - startTime)/1000;
    }

    private String getTime() {
        long ms = System.currentTimeMillis() - startTime;
//        return  String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(ms),
//                TimeUnit.MILLISECONDS.toMinutes(ms) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(ms)),
//                TimeUnit.MILLISECONDS.toSeconds(ms) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(ms)));

        long second = (ms / 1000) % 60;
        long minute = (ms / (1000 * 60)) % 60;
        long hour = (ms / (1000 * 60 * 60)) % 24;

        StringBuilder builder = new StringBuilder();
        builder.append(hour);
        builder.append(ZNAK);
        builder.append(minute);
        builder.append(ZNAK);
        builder.append(second);
        return builder.toString();
    }
}
