package me.admund.ld33monster;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import me.admund.framework.GameUtils;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.scenes.AbstractScene;
import me.admund.ld33monster.enemies.*;
import me.admund.ld33monster.gui.ScoreGui;
import me.admund.ld33monster.physics.Collisions;
import me.admund.ld33monster.physics.Ground;
import me.admund.ld33monster.physics.Lava;
import me.admund.ld33monster.physics.MonsterReuseFactory;
import me.admund.ld33monster.player.Bottle;
import me.admund.ld33monster.player.Player;

/**
 * Created by admund on 2015-08-22.
 */
public class GameScene extends AbstractScene {
    private static final String GROUP_NAME = "MAIN";

    public static final float screenW = PhysicsWorld.BOX_SCREEN_WIDTH;
    public static final float screenH = PhysicsWorld.BOX_SCREEN_HEIGHT;
    public static final float mapW = screenW * 2;
    public static final float mapH = screenH * 2;

    @Override
    public void create() {
        createGroups(new String[]{GROUP_NAME});
        Logic.container = getGroup(GROUP_NAME);

        Logic.world = new PhysicsWorld(new MonsterReuseFactory());
        Logic.world.setContactListner(new Collisions());

        createGameMap();

        Logic.player = (Player)Logic.world.getPhysicsObject(Player.class.toString());
        Logic.player.init();
        addActor(GROUP_NAME, Logic.player);

        Bottle bottle = (Bottle)Logic.world.getPhysicsObject(Bottle.class.toString());
        bottle.init();
        addActor(GROUP_NAME, bottle);

        Logic.enemiesList = new EnemiesList();
        createEnemies();
        createPatrols();

        guiStage.addActor(new ScoreGui(guiStage));

        GameUtils.soundsManager.playMain();
    }

    @Override
    public void act(float deltaTime) {
        Logic.enemiesList.reset();
        Logic.world.step(deltaTime);
        super.act(deltaTime);
        updateCamera();
    }

    @Override
    public void draw(Batch batch) {
        super.draw(batch);
        //Logic.world.debugRender(getCurrentCamera());
    }

    private void createEnemies() {
        for(int i=0; i<30; i++) {
            Enemy tmp = (Enemy)Logic.world.getPhysicsObject(Enemy.class.toString());
            tmp.init(EnemyUtils.getRandPos(mapW),
                    EnemyUtils.getRandPos(mapH));
            addActor(GROUP_NAME, tmp);
            Logic.enemiesList.addEnemy(tmp);
        }
    }

    private void createPatrols() {
        for(int i=0; i<1; i++) {
            PatrolLeaderEnemy patrolLeader = (PatrolLeaderEnemy)Logic.world.getPhysicsObject(
                    PatrolLeaderEnemy.class.toString());
            patrolLeader.init(EnemyUtils.getRandPos(mapW), EnemyUtils.getRandPos(mapH));
            addActor(GROUP_NAME, patrolLeader);
            Logic.enemiesList.addEnemy(patrolLeader);

            createMembers(patrolLeader, 2);
            createMembers(patrolLeader, 2);
        }
    }

    private void createMembers(Enemy leader, int moreLevel) {
        PatrolMemeberEnemy patrolMember = (PatrolMemeberEnemy)Logic.world.getPhysicsObject(
                PatrolMemeberEnemy.class.toString());
        patrolMember.init(EnemyUtils.getRandPos(mapW), EnemyUtils.getRandPos(mapH), leader);
        addActor(GROUP_NAME, patrolMember);
        Logic.enemiesList.addEnemy(patrolMember);

        if(moreLevel > 0) {
            moreLevel--;
            createMembers(patrolMember, moreLevel);
            createMembers(patrolMember, moreLevel);
        }
    }

    private void createGameMap() {
        Lava lavaLeft = (Lava) Logic.world.getPhysicsObject(Lava.class.toString());
        lavaLeft.init(-screenW * .25f, mapH * .5f, screenW * .5f, mapH + screenH);
        addActor(GROUP_NAME, lavaLeft);

        Lava lavaRight = (Lava) Logic.world.getPhysicsObject(Lava.class.toString());
        lavaRight.init(mapW + screenW * .25f, mapH * .5f, screenW * .5f, mapH + screenH);
        addActor(GROUP_NAME, lavaRight);

        Lava lavaTop = (Lava) Logic.world.getPhysicsObject(Lava.class.toString());
        lavaTop.init(mapW * .5f, mapH + screenH * .25f, mapW, screenH * .5f);
        addActor(GROUP_NAME, lavaTop);

        Lava lavaDown = (Lava) Logic.world.getPhysicsObject(Lava.class.toString());
        lavaDown.init(mapW * .5f, -screenH * .25f, mapW, screenH * .5f);
        addActor(GROUP_NAME, lavaDown);

        Ground ground = new Ground(AssetsList.GROUND);
        ground.init(0, 0, mapW, mapH);
        addActor(GROUP_NAME, ground);

        float size = 8;
        Ground cliffUp = new Ground(AssetsList.CLIFF_UP);
        cliffUp.init(0, -size, mapW, size);
        addActor(GROUP_NAME, cliffUp);

        Ground cliffDown = new Ground(AssetsList.CLIFF_DOWN);
        cliffDown.init(0, -size * 2, mapW , size);
        addActor(GROUP_NAME, cliffDown);
    }

    private void updateCamera() {
        GameUtils.currentCamera = getCurrentCamera();
        Vector2 playerPos = Logic.player.getPosition();
        Vector3 cameraPos = getCurrentCamera().position;
        getCurrentCamera().translate(playerPos.x * PhysicsWorld.BOX_TO_SCREEN - cameraPos.x,
                playerPos.y * PhysicsWorld.BOX_TO_SCREEN - cameraPos.y, 0);
    }
}
