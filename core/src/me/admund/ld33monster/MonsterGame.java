package me.admund.ld33monster;

import me.admund.framework.achievements.IAchievementsProvider;
import me.admund.framework.assets.FrameworkAssetsManager;
import me.admund.framework.draw.particle.FrameworkParticleManager;
import me.admund.framework.game.AbstractGame;
import me.admund.framework.scenes.IScene;
import me.admund.framework.scenes.RegistrationScene;
import me.admund.framework.sounds.FrameworkSoundsManager;

/**
 * Created by admund on 2015-08-22.
 */
public class MonsterGame extends AbstractGame {

    public MonsterGame(IAchievementsProvider achievementsProvider) {
        super(achievementsProvider);
        Logic.achievementsProvider = achievementsProvider;
    }

    @Override
    protected FrameworkAssetsManager createAssetManager() {
        return new MonsterAssetManager();
    }

    @Override
    protected FrameworkSoundsManager createSoundsManager() {
        return new FrameworkSoundsManager() {
            @Override
            public void init() {
                setMainTheme(AssetsList.mainTheme);

                addSound(AssetsList.shout);
                addSound(AssetsList.screm);
            }
        };
    }

    @Override
    protected FrameworkParticleManager createParticleManager() {
        return new FrameworkParticleManager() {
            @Override
            public void init() {}
        };
    }

    @Override
    protected IScene createFirstScene() {
        return new RegistrationScene();//new GameScene();
    }
}
