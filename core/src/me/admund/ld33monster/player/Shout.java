package me.admund.ld33monster.player;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import me.admund.framework.draw.animations.AnimationState;
import me.admund.framework.draw.animations.AnimationUtils;
import me.admund.framework.draw.holders.AnimationSpriteHolder;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.utils.TimeBoolean;
import me.admund.ld33monster.AssetsList;
import me.admund.ld33monster.enemies.MonsterPhysicsObject;
import me.admund.ld33monster.physics.CollisionFilters;

/**
 * Created by admund on 2015-08-22.
 */
public class Shout extends MonsterPhysicsObject {
    private float shoutW = 30f;
    private float shoutH = 15f;

    float time = .2f;
    private TimeBoolean validTime = new TimeBoolean(true, time);

    public void init(float posX, float posY, float rotation) {
        super.init();
        setCurrentPos(posX, posY, rotation);
        setSize(shoutW, shoutH);
        //setSpriteHolder(new SimpleSpriteHolder(AssetsList.SHOUT, shoutW * .5f, 0), false);
        setSpriteHolder(new AnimationSpriteHolder(shoutW * .5f, 0).addAnimation(AnimationState.IDLE,
                AnimationUtils.createAnimation(time / 3, AssetsList.WAVE, Animation.PlayMode.NORMAL)), false);

        Vector2[] verticles = new Vector2[4];
        verticles[0] = new Vector2(0,0);
        verticles[1] = new Vector2(shoutW * .9f, shoutH * -.5f);
        verticles[2] = new Vector2(shoutW, 0);
        verticles[3] = new Vector2(shoutW * .9f, shoutH * .5f);
        PhysicsUtils.updateRectShape(getShape(), verticles);

        setOrigin(0, shoutH * .5f);

        validTime.change();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        validTime.act(delta);
        if(validTime.getValue()) {
            prepereToReuse();
        }
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.active = true;
        def.fixedRotation = true;
        def.type = BodyDef.BodyType.DynamicBody;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultPolygonShape();
        def.density = 1000;
        def.filter.categoryBits = CollisionFilters.CATEGORY_SHOUT;
        def.filter.maskBits = CollisionFilters.MASK_SHOUT;
        return def;
    }
}
