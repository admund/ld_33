package me.admund.ld33monster.player;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.animations.AnimationState;
import me.admund.framework.draw.animations.AnimationUtils;
import me.admund.framework.draw.holders.AnimationSpriteHolder;
import me.admund.framework.draw.holders.ISpriteHolder;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.utils.TimeBoolean;
import me.admund.ld33monster.AssetsList;
import me.admund.ld33monster.GameScene;
import me.admund.ld33monster.Logic;
import me.admund.ld33monster.enemies.MonsterPhysicsObject;
import me.admund.ld33monster.physics.CollisionFilters;

/**
 * Created by admund on 2015-08-22.
 */
public class Player extends MonsterPhysicsObject {

    private PlayerController controller = null;
    protected TimeBoolean canShout = new TimeBoolean(true, 2);
    protected TimeBoolean drinkBottle = new TimeBoolean(false, 5);

    @Override
    public void init() {
        super.init();
        setCurrentPos(GameScene.mapW / 2, GameScene.mapH / 2);
        setSize(6, 9);
        setSpriteHolder(createAnimationSpriteHolder(), false);

        controller = new PlayerController(this);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        controller.act(delta);
        canShout.act(delta);
        drinkBottle.act(delta);
    }

    public float getShoutPercent() {
        return 1  - canShout.getPercent();
    }

    public void shout(Vector2 touchPos) {
        float angle = getTouchAngle(touchPos);
        Shout shout = (Shout) Logic.world.getPhysicsObject(Shout.class.toString());
        Vector2 playerPos = getPosition();
        shout.init(playerPos.x, playerPos.y, angle);
        Logic.container.addActor(shout);

        if(!drinkBottle.getValue()) {
            canShout.change();
        }

        GameUtils.soundsManager.playSound(AssetsList.shout);
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {
        Object obj = PhysicsUtils.getObject(contact, isObjectA);
        if(obj instanceof Bottle) {
            drinkBottle.forceChange();
            canShout.reset();
        }
    }

    @Override
    public void endContact(Contact contact, boolean isObjectA) {}

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.active = true;
        def.fixedRotation = true;
        def.type = BodyDef.BodyType.DynamicBody;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultPolygonShape();
        def.filter.categoryBits = CollisionFilters.CATEGORY_PLAYER;
        def.filter.maskBits = CollisionFilters.MASK_PLAYER;
        return def;
    }

    private float getTouchAngle(Vector2 touchPos) {
        float angle = touchPos.sub(getPosition()).nor().angleRad();
        return angle;
    }

    private ISpriteHolder createAnimationSpriteHolder() {
        AnimationSpriteHolder holder = new AnimationSpriteHolder();
        holder.addAnimation(AnimationState.MOVE_RIGHT,
                AnimationUtils.createAnimation(.5f, AssetsList.PLAYER_MOVE_RIGHT, Animation.PlayMode.LOOP));
        holder.addAnimation(AnimationState.MOVE_LEFT,
                AnimationUtils.createAnimation(.5f, AssetsList.PLAYER_MOVE_LEFT, Animation.PlayMode.LOOP));
        holder.addAnimation(AnimationState.IDLE,
                AnimationUtils.createAnimation(.9f, AssetsList.PLAYER_IDLE, Animation.PlayMode.LOOP));
        return holder;
    }
}
