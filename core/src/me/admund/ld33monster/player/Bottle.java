package me.admund.ld33monster.player;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import me.admund.framework.draw.holders.SimpleSpriteHolder;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.physics.objects.PhysicsRect;
import me.admund.ld33monster.AssetsList;
import me.admund.ld33monster.GameScene;
import me.admund.ld33monster.enemies.EnemyUtils;
import me.admund.ld33monster.physics.CollisionFilters;

/**
 * Created by admund on 2015-08-23.
 */
public class Bottle extends PhysicsRect {

    private boolean reset = false;

    @Override
    public void init() {
        super.init(EnemyUtils.getRandPos(GameScene.mapW), EnemyUtils.getRandPos(GameScene.mapH),
                1.8f, 3.8f);
        setSpriteHolder(new SimpleSpriteHolder(AssetsList.BOTTLE), false);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(reset) {
            init();
            reset = false;
        }
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {
        Object obj = PhysicsUtils.getObject(contact, isObjectA);
        if(obj instanceof Player) {
            reset = true;
        }
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef fixtureDef = super.getFixtureDef();
        fixtureDef.filter.categoryBits = CollisionFilters.CATEGORY_BOTTLE;
        fixtureDef.filter.maskBits = CollisionFilters.MASK_BOTTLE;
        return fixtureDef;
    }
}
