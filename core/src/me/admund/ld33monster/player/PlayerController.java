package me.admund.ld33monster.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.math.Vector3;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.animations.AnimationState;
import me.admund.framework.physics.PhysicsWorld;

/**
 * Created by admund on 2015-01-11.
 */
public class PlayerController {
    private static final float MAX_SPEED = 15f;

    private Player player = null;

    private boolean goUp = false;
    private boolean goDown = false;
    private boolean goLeft = false;
    private boolean goRight = false;

    private PlayerInput input = null;

    public PlayerController(Player player) {
        this.player = player;
        input = new PlayerInput(this);
        ((InputMultiplexer) Gdx.input.getInputProcessor()).addProcessor(1, input);
    }

    public void setMove(MoveEnum move, boolean start) {
        if (move == MoveEnum.UP) {
            goUp = start;
        } else if (move == MoveEnum.DOWN) {
            goDown = start;
        } else if (move == MoveEnum.LEFFT) {
            goLeft = start;
        } else if (move == MoveEnum.RIGHT) {
            goRight = start;
        }
    }

    public void act(float deltaTime) {
        updateVelocity();
    }

    protected void updateVelocity() {
        float newVeloX = (goRight ? MAX_SPEED : 0) + (goLeft ? -MAX_SPEED : 0);
        float newVeloY = (goUp ? MAX_SPEED : 0) + (goDown ? -MAX_SPEED : 0);
        player.changeAnimationState(getProperAnimationState(newVeloX, newVeloY));
        player.getBody().setLinearVelocity(newVeloX, newVeloY);
    }

    private AnimationState getProperAnimationState(float veloX, float veloY) {
        AnimationState result = null;
        if(veloX == 0 && veloY == 0) {
            return AnimationState.IDLE;
//        } else if( veloX < 0) {
//            result =  AnimationState.MOVE_LEFT;
//        } else {
//            result =  AnimationState.MOVE_RIGHT;
//        }
        }
        Vector3 mousePos = GameUtils.currentCamera.unproject(new Vector3(input.mouseScreenPosX, 0, 0))
                .scl(PhysicsWorld.SCREEN_TO_BOX);
        if(mousePos.x < player.getPosition().x) {
            result =  AnimationState.MOVE_LEFT;
        } else {
            result =  AnimationState.MOVE_RIGHT;
        }
        return result;
    }
}
