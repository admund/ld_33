package me.admund.ld33monster.player;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import me.admund.framework.GameUtils;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.scenes.ScenesManager;
import me.admund.ld33monster.GameScene;
import me.admund.ld33monster.Logic;

/**
 * Created by admund on 2015-01-11.
 */
public class PlayerInput extends InputAdapter {

    private PlayerController playerController = null;
    public PlayerInput(PlayerController playerController) {
        this.playerController = playerController;
    }

    protected float mouseScreenPosX = 0;

    @Override
    public boolean keyDown(int keycode) {
        return checkKeys(keycode, true);
    }

    @Override
    public boolean keyUp(int keycode) {
        return checkKeys(keycode, false);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if(canShout()) {
            Vector3 tmp = GameUtils.currentCamera.unproject(new Vector3(screenX, screenY, 0))
                    .scl(PhysicsWorld.SCREEN_TO_BOX);
            Logic.player.shout(new Vector2(tmp.x, tmp.y));
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        mouseScreenPosX = screenX;
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    private boolean checkKeys(int keycode, boolean down) {
        boolean result = false;
        if(keycode == Input.Keys.UP || keycode == Input.Keys.W) {
            playerController.setMove(MoveEnum.UP, down);
            result = true;
        } else if(keycode == Input.Keys.DOWN || keycode == Input.Keys.S) {
            playerController.setMove(MoveEnum.DOWN, down);
            result = true;
        } else if(keycode == Input.Keys.LEFT || keycode == Input.Keys.A) {
            playerController.setMove(MoveEnum.LEFFT, down);
            result = true;
        } else if(keycode == Input.Keys.RIGHT || keycode == Input.Keys.D) {
            playerController.setMove(MoveEnum.RIGHT, down);
            result = true;

        } else if(keycode == Input.Keys.R && Logic.enemiesList.getEnemies() == 0) {
            ScenesManager.inst().pop();
            ScenesManager.inst().push(new GameScene(), true);
        }
        return result;
    }

    private boolean canShout() {
        return Logic.player.canShout.getValue();
    }
}
