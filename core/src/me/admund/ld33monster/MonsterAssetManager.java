package me.admund.ld33monster;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import me.admund.framework.assets.FrameworkAssetsManager;

/**
 * Created by admund on 2015-08-22.
 */
public class MonsterAssetManager extends FrameworkAssetsManager {

    @Override
    public void init() {
        mainAtlas = get(AssetsList.MAIN_ATLAS_NAME, TextureAtlas.class);
    }

    @Override
    public void load() {
        load(AssetsList.MAIN_ATLAS_NAME, TextureAtlas.class);

        // font
        load(AssetsList.basicFont, BitmapFont.class);

        // sounds
        load(AssetsList.mainTheme, Music.class);
        load(AssetsList.shout, Sound.class);
        load(AssetsList.screm, Sound.class);
    }
}
