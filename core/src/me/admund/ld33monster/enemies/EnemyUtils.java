package me.admund.ld33monster.enemies;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * Created by admund on 2015-08-22.
 */
public class EnemyUtils {

    public static void randNewVelocity(Body body) {
        Vector2 velocity = body.getLinearVelocity();
        velocity.set(MathUtils.random() * MathUtils.randomSign(), MathUtils.random() * MathUtils.randomSign())
                .nor().scl(Enemy.MAX_SPEED);
        body.setLinearVelocity(velocity);
    }

    public static float getRandPos(float max) {
        return max * .1f + max * .8f * MathUtils.random();
    }

    public static Vector2 getRandPos(float maxX, float maxY) {
        return new Vector2(getRandPos(maxX), getRandPos(maxY));
    }

    public static Vector2 getVelocityToPoint(Vector2 dest, Vector2 myPos, float maxVelocity) {
        return dest.cpy().sub(myPos).nor().scl(maxVelocity);
    }
}
