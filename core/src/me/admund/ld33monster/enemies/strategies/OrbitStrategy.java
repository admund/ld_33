package me.admund.ld33monster.enemies.strategies;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import me.admund.ld33monster.enemies.Enemy;
import me.admund.ld33monster.enemies.EnemyUtils;

/**
 * Created by admund on 2015-08-22.
 */
public class OrbitStrategy implements IStrategy {
    public static final String START_POS_X = "startPosX" + OrbitStrategy.class.toString();
    public static final String START_POS_Y = "startPosY" + OrbitStrategy.class.toString();

    private static final float maxRange2 = 15 * 15;

    private long lastChange = 0;
    private long changeInterval = 200;
    private Vector2 startPos = null;

    @Override
    public IStrategy init(StrategyAttributes attributes) {
        float startPosX = (Float)attributes.get(START_POS_X);
        float startPosY = (Float)attributes.get(START_POS_Y);
        startPos = new Vector2(startPosX, startPosY);
        return this;
    }

    @Override
    public boolean act(float delta, Enemy enemy) {
        if(canChange()) {
            Vector2 currentPos = enemy.getPosition();
            float dist2 = Vector2.dst2(startPos.x, startPos.y, currentPos.x, currentPos.y);
            double rand = MathUtils.random();
            if ((dist2 / maxRange2) < rand) {
                EnemyUtils.randNewVelocity(enemy.getBody());
            } else {
                Vector2 newVelocity = startPos.cpy().sub(currentPos).nor().scl(Enemy.MAX_SPEED);
                enemy.getBody().setLinearVelocity(newVelocity);
            }
            return true;
        }
        return false;
    }

    private boolean canChange() {
        if(System.currentTimeMillis() - lastChange > changeInterval) {
            lastChange = System.currentTimeMillis();
            return true;
        }
        return false;
    }
}
