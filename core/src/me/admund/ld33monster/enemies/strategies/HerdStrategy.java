package me.admund.ld33monster.enemies.strategies;

import com.badlogic.gdx.math.Vector2;
import me.admund.ld33monster.Logic;
import me.admund.ld33monster.enemies.Enemy;
import me.admund.ld33monster.enemies.EnemyUtils;

/**
 * Created by admund on 2015-08-22.
 */
public class HerdStrategy implements IStrategy {

    @Override
    public IStrategy init(StrategyAttributes attributes) {
        return this;
    }

    @Override
    public boolean act(float delta, Enemy enemy) {
        Vector2 avgPos = Logic.enemiesList.getAvegPos();
        enemy.getBody().setLinearVelocity(
                EnemyUtils.getVelocityToPoint(avgPos, enemy.getPosition(), Enemy.MAX_SPEED));
        return false;
    }
}
