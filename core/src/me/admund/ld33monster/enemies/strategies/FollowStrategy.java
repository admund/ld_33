package me.admund.ld33monster.enemies.strategies;

import com.badlogic.gdx.math.Vector2;
import me.admund.ld33monster.enemies.Enemy;
import me.admund.ld33monster.enemies.EnemyUtils;

/**
 * Created by admund on 2015-08-22.
 */
public class FollowStrategy implements IStrategy {
    public static final String TO_FOLOW = "toFollow" + FollowStrategy.class.toString();

    private float minRange2 =  10 * 10;
    private Enemy toFollow = null;

    @Override
    public IStrategy init(StrategyAttributes attributes) {
        toFollow = (Enemy)attributes.get(TO_FOLOW);
        return this;
    }

    @Override
    public boolean act(float delta, Enemy enemy) {
        float dist2 = Vector2.dst2(toFollow.getPosition().x, toFollow.getPosition().y,
                enemy.getPosition().x, enemy.getPosition().y);
        if(dist2 > minRange2) {
            if(!toFollow.canBeReuse()) {
                enemy.getBody().setLinearVelocity(
                        EnemyUtils.getVelocityToPoint(toFollow.getPosition(), enemy.getPosition(), Enemy.MAX_SPEED));
            }
        } else {
            enemy.getBody().setLinearVelocity(Vector2.Zero);
        }
        return true;
    }
}
