package me.admund.ld33monster.enemies.strategies;

import com.badlogic.gdx.utils.Array;
import me.admund.ld33monster.enemies.Enemy;

/**
 * Created by admund on 2015-08-22.
 */
public class StrategyContainer {
    private Array<IStrategy> strategyList = new Array<IStrategy>();

    public void addStrategy(IStrategy strategy) {
        strategyList.add(strategy);
    }

    public void act(float delta, Enemy enemy) {
        for(int i=0; i<strategyList.size; i++) {
            if(strategyList.get(i).act(delta, enemy)) {
                return;
            }
        }
    }
}
