package me.admund.ld33monster.enemies.strategies;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import me.admund.ld33monster.GameScene;
import me.admund.ld33monster.enemies.Enemy;
import me.admund.ld33monster.enemies.EnemyUtils;

/**
 * Created by admund on 2015-08-22.
 */
public class PathStrategy implements IStrategy {

    private Array<Vector2> pathPointsList = new Array<Vector2>();
    private int pointToGo = 0;
    private float minRange2 = 2 * 2;

    @Override
    public IStrategy init(StrategyAttributes attributes) {
        int pointCnt = 2 + MathUtils.random(4);
        for(int i=0; i<pointCnt; i++) {
            pathPointsList.add(EnemyUtils.getRandPos(GameScene.mapW, GameScene.mapH));
        }
        return this;
    }

    @Override
    public boolean act(float delta, Enemy enemy) {
        Vector2 pathPoint = pathPointsList.get(pointToGo);
        float dist2 = Vector2.dst2(pathPoint.x, pathPoint.y, enemy.getPosition().x, enemy.getPosition().y);
        if(dist2 < minRange2) {
            pointToGo++;
            if(pointToGo >= pathPointsList.size) {
                pointToGo = 0;
            }

            pathPoint = pathPointsList.get(pointToGo);
//            enemy.getBody().setLinearVelocity(
//                    EnemyUtils.getVelocityToPoint(pathPoint, enemy.getPosition(), Enemy.MAX_SPEED));
        }
        enemy.getBody().setLinearVelocity(
                EnemyUtils.getVelocityToPoint(pathPoint, enemy.getPosition(), Enemy.MAX_SPEED));
        return true;
    }
}
