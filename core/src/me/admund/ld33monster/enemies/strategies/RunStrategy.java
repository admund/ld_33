package me.admund.ld33monster.enemies.strategies;

import com.badlogic.gdx.math.Vector2;
import me.admund.framework.utils.TimeBoolean;
import me.admund.ld33monster.Logic;
import me.admund.ld33monster.enemies.Enemy;

/**
 * Created by admund on 2015-08-22.
 */
public class RunStrategy implements IStrategy {
    public static final String MAX_RANGE = "maxRange" + RunStrategy.class.toString();

    private TimeBoolean delay = new TimeBoolean(true, .06f);
    private float dengerZone2 = 15f * 15f;
    private float notSafeZone2 = 20f * 20f;
    private int sign = 0;

    @Override
    public IStrategy init(StrategyAttributes attributes) {
        //maxRange2 = (Float)attributes.get(MAX_RANGE, maxRange2);
        return this;
    }

    @Override
    public boolean act(float delta, Enemy enemy) {
        delay.act(delta);
        Vector2 currentPos = enemy.getPosition();
        Vector2 playerPos = Logic.player.getPosition();
        float dist2 = Vector2.dst2(playerPos.x, playerPos.y, currentPos.x, currentPos.y);
        if(dist2 < notSafeZone2) {
            if(dist2 < dengerZone2) {

//                    Vector2 newVelocity = currentPos.sub(playerPos).nor().scl(Enemy.MAX_SPEED * 1.5f);
//                    sign = (sign == 0) ? MathUtils.randomSign() : sign;
//                    newVelocity.set(sign * -newVelocity.y + .5f * newVelocity.x,
//                            sign * newVelocity.x + .5f * newVelocity.y);
//                    enemy.getBody().setLinearVelocity(newVelocity);

                    Vector2 oldVelocity = enemy.getBody().getLinearVelocity();
                    Vector2 newVelocity = currentPos.sub(playerPos).nor().scl(Enemy.MAX_SPEED);
                    Vector2 superNewVelocity = oldVelocity.add(newVelocity).nor().scl(Enemy.MAX_SPEED);
                    enemy.getBody().setLinearVelocity(superNewVelocity);

            }
            return true;
        }
        return false;
    }
}
