package me.admund.ld33monster.enemies.strategies;

import me.admund.ld33monster.enemies.Enemy;

/**
 * Created by admund on 2015-08-22.
 */
public interface IStrategy {
    IStrategy init(StrategyAttributes attributes);
    boolean act(float delta, Enemy enemy);
}
