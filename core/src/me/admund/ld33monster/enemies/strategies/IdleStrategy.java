package me.admund.ld33monster.enemies.strategies;

import com.badlogic.gdx.math.Vector2;
import me.admund.ld33monster.enemies.Enemy;

/**
 * Created by admund on 2015-08-22.
 */
public class IdleStrategy implements IStrategy {

    @Override
    public IStrategy init(StrategyAttributes attributes) {
        return this;
    }

    @Override
    public boolean act(float delta, Enemy enemy) {
        // DO NOTHING :)
        enemy.getBody().setLinearVelocity(Vector2.Zero);
        return false;
    }
}
