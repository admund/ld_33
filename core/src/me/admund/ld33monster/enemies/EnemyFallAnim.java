package me.admund.ld33monster.enemies;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.animations.AnimationState;
import me.admund.framework.draw.holders.AnimationSpriteHolder;

/**
 * Created by admund on 2015-08-23.
 */
public class EnemyFallAnim extends DrawObject {

    public EnemyFallAnim(Animation animation) {
        setSpriteHolder(new AnimationSpriteHolder().addAnimation(AnimationState.IDLE, animation), false);
    }

    public Actor init(float startPosX, float startPosY, Vector2 fallDirection2) {
        setPosition(startPosX, startPosY);
        setSize(Enemy.ENEMY_SIZE * 2, Enemy.ENEMY_SIZE * 2);
        setOrigin(Align.center);
        addAction(createAnim(fallDirection2));
        return this;
    }

    private float animTime1 = .5f;
    private float animTime2 = 3;
    private float fallDistance = 20;
    private Action createAnim(Vector2 fallDirection) {
        return Actions.sequence(
                    Actions.parallel(
                            Actions.moveBy(fallDirection.x * fallDistance, fallDirection. y * fallDistance, animTime1),
                            Actions.scaleTo(1.1f, 1.1f, animTime1)),
                    Actions.parallel(
                            Actions.moveBy(fallDirection.x * fallDistance / 2, fallDirection.y * fallDistance / 2, animTime2),
                            Actions.scaleTo(0, 0, animTime2),
                            Actions.rotateBy(960, animTime2)),
                    Actions.removeActor());
    }
}
