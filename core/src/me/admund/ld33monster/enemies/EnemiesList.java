package me.admund.ld33monster.enemies;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by admund on 2015-08-22.
 */
public class EnemiesList {
    private Array<Enemy> enemyList = new Array<Enemy>();

    private Vector2 avgPos = null;

    public void reset() {
        avgPos = null;
    }

    public void addEnemy(Enemy enemy) {
        if(!enemyList.contains(enemy, true)) {
            enemyList.add(enemy);
        }
    }

    public Vector2 getAvegPos() {
        if(avgPos == null) {
            int cnt = 0;
            float posX = 0;
            float posY = 0;
            for (int i = 0; i < enemyList.size; i++) {
                Enemy tmp = enemyList.get(i);
                if (!tmp.canBeReuse()) {
                    cnt++;
                    posX += tmp.getPosition().x;
                    posY += tmp.getPosition().y;
                }
            }
            avgPos = new Vector2(posX/cnt, posY/cnt);
        }
        return avgPos;
    }

    public int getEnemies() {
        int cnt = 0;
        for (int i = 0; i < enemyList.size; i++) {
            Enemy tmp = enemyList.get(i);
            if (!tmp.canBeReuse()) {
                cnt++;
            }
        }
        return cnt;
    }
}
