package me.admund.ld33monster.enemies;

import me.admund.ld33monster.enemies.strategies.IStrategy;
import me.admund.ld33monster.enemies.strategies.PathStrategy;

/**
 * Created by admund on 2015-08-22.
 */
public class PatrolLeaderEnemy extends Enemy {

    @Override
    protected IStrategy getBasicStrategy() {
        return new PathStrategy();
    }
}
