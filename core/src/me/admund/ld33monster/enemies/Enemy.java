package me.admund.ld33monster.enemies;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.animations.AnimationState;
import me.admund.framework.draw.animations.AnimationUtils;
import me.admund.framework.draw.holders.AnimationSpriteHolder;
import me.admund.framework.draw.holders.ISpriteHolder;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.utils.TimeBoolean;
import me.admund.ld33monster.AssetsList;
import me.admund.ld33monster.Logic;
import me.admund.ld33monster.enemies.strategies.*;
import me.admund.ld33monster.physics.CollisionFilters;
import me.admund.ld33monster.physics.Lava;
import me.admund.ld33monster.player.Shout;

/**
 * Created by admund on 2015-08-22.
 */
public class Enemy extends MonsterPhysicsObject {
    public static float MAX_SPEED = 20f;
    public static final float ENEMY_SIZE = 2;

    private StrategyContainer strategy = null;
    private TimeBoolean isFearful = new TimeBoolean(false, .5f);

    private boolean isBoy = false;

    public void init(float posX, float posY) {
        super.init();
        setCurrentPos(posX, posY);
        setSize(ENEMY_SIZE);
        setSpriteHolder(createAnimationSpriteHolder(), false);

        StrategyAttributes attr = createStrategyAttributes();
        strategy = new StrategyContainer();
        strategy.addStrategy(new RunStrategy().init(attr));
        strategy.addStrategy(getBasicStrategy().init(attr));
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        isFearful.act(delta);
        if(!isFearful.getValue()) {
            strategy.act(delta, this);
        }
        updateAnimation();
    }

    @Override
    public void beginContact(Contact contact, boolean isObjectA) {
        Object obj = PhysicsUtils.getObject(contact, isObjectA);
        if(obj instanceof Lava) {
            getInfo().setToReuse();
            createEnemyFallAnimation();
            GameUtils.soundsManager.playSound(AssetsList.screm);

        } else if(obj instanceof Shout) {
            if(!isFearful.getValue()) {
                isFearful.change();

                Vector2 tmp = getPosition().cpy().sub(Logic.player.getPosition()).nor().scl(Enemy.MAX_SPEED * 3);
                getBody().setLinearVelocity(tmp);
            }
        } else if(obj instanceof Enemy) {
            if(((Enemy)obj).isFearful.getValue() || isFearful.getValue()) {
                contact.setEnabled(false);
            }
        }
    }

    @Override
    public BodyDef getBodyDef() {
        BodyDef def = new BodyDef();
        def.active = true;
        def.fixedRotation = true;
        def.type = BodyDef.BodyType.DynamicBody;
        return def;
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultCircleShape();
        def.friction = 0;
        def.filter.categoryBits = CollisionFilters.CATEGORY_ENEMIES;
        def.filter.maskBits = CollisionFilters.MASK_ENEMIES;
        return def;
    }

    protected IStrategy getBasicStrategy() {
        float rand = MathUtils.random();
//        if(rand < .25f) {
//            return new OrbitStrategy();
//        } else if (rand < .5f){
//            return new IdleStrategy();
//        } else if (rand < .75f){
//            return new HerdStrategy();
//        } else {
//            return new PathStrategy();
//        }

        if(rand < .33f) {
            return new OrbitStrategy();
        } else if (rand < .66f){
            return new IdleStrategy();
//        } else if (rand < .75f){
//            return new HerdStrategy();
        } else {
            return new PathStrategy();
        }
    }

    protected StrategyAttributes createStrategyAttributes() {
        StrategyAttributes attr = new StrategyAttributes();
        attr.put(OrbitStrategy.START_POS_X, getPosition().x);
        attr.put(OrbitStrategy.START_POS_Y, getPosition().y);
        return attr;
    }

    private void updateAnimation() {
        Vector2 velo = getBody().getLinearVelocity();
        if(velo.x == 0 && velo.y == 0) {
            changeAnimationState(AnimationState.IDLE);
        } else if(velo.x < 0) {
            changeAnimationState(AnimationState.MOVE_LEFT);
        } else {
            changeAnimationState(AnimationState.MOVE_RIGHT);
        }
    }

    private ISpriteHolder createAnimationSpriteHolder() {
        AnimationSpriteHolder holder = new AnimationSpriteHolder();
        isBoy = MathUtils.randomBoolean();
        String textureMoveLeft = isBoy ? AssetsList.ENEMY_B_MOVE_LEFT : AssetsList.ENEMY_G_MOVE_LEFT;
        String textureMoveRight = isBoy ? AssetsList.ENEMY_B_MOVE_RIGHT : AssetsList.ENEMY_G_MOVE_RIGHT;

        holder.addAnimation(AnimationState.MOVE_LEFT,
                AnimationUtils.createAnimation(.5f, textureMoveLeft, Animation.PlayMode.LOOP));
        holder.addAnimation(AnimationState.MOVE_RIGHT,
                AnimationUtils.createAnimation(.5f, textureMoveRight, Animation.PlayMode.LOOP));
        return holder;
    }

    private void createEnemyFallAnimation() {
        String textuteName = isBoy ? AssetsList.ENEMY_B_FALL : AssetsList.ENEMY_G_FALL;
        Logic.container.addActor(new EnemyFallAnim(AnimationUtils.createAnimation(.5f, textuteName, Animation.PlayMode.LOOP))
                .init(getPosition().x, getPosition().y, getBody().getLinearVelocity().nor()));
    }
}
