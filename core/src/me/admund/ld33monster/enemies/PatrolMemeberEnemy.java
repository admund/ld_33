package me.admund.ld33monster.enemies;

import me.admund.ld33monster.enemies.strategies.FollowStrategy;
import me.admund.ld33monster.enemies.strategies.IStrategy;
import me.admund.ld33monster.enemies.strategies.StrategyAttributes;

/**
 * Created by admund on 2015-08-22.
 */
public class PatrolMemeberEnemy extends Enemy {

    private Enemy toFollow = null;

    public void init(float posX, float posY, Enemy enemy) {
        toFollow = enemy;
        super.init(posX, posY);
    }

    @Override
    protected IStrategy getBasicStrategy() {
        return new FollowStrategy();
    }

    @Override
    protected StrategyAttributes createStrategyAttributes() {
        StrategyAttributes attr = super.createStrategyAttributes();
        attr.put(FollowStrategy.TO_FOLOW, toFollow);
        return attr;
    }
}
