package me.admund.ld33monster.enemies;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by admund on 2015-08-22.
 */
public class EnemyStrategy {
    private Vector2 startPos = null;
    private float maxRange2 = 10f * 10f;

    public EnemyStrategy(Vector2 startPos) {
        this.startPos = startPos;
    }

    public void setNewSpeed(Enemy enemy) {
        Vector2 currentPos = enemy.getPosition();
        float dist2 = Vector2.dst2(startPos.x, startPos.y, currentPos.x, currentPos.y);
        if((dist2 / maxRange2) > Math.random()) {
            EnemyUtils.randNewVelocity(enemy.getBody());
        }
    }
}
