package me.admund.ld33monster.enemies;

import me.admund.framework.physics.NoneType;
import me.admund.framework.physics.PhysicsObject;

/**
 * Created by admund on 2015-08-22.
 */
public abstract class MonsterPhysicsObject extends PhysicsObject {
    public MonsterPhysicsObject() {
        super(new NoneType(true));
    }
}
