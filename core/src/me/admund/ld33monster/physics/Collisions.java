package me.admund.ld33monster.physics;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.PhysicsUtils;

/**
 * Created by admund on 2015-08-22.
 */
public class Collisions implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Object objectA = PhysicsUtils.getObject(contact.getFixtureA().getBody());
        if(objectA != null && objectA instanceof PhysicsObject) {
            ((PhysicsObject)objectA).beginContact(contact, true);
        }

        Object objectB = PhysicsUtils.getObject(contact.getFixtureB().getBody());
        if(objectB != null && objectB instanceof PhysicsObject) {
            ((PhysicsObject)objectB).beginContact(contact, false);
        }
        //System.out.println("contact " + objectA + " " + objectB);
    }

    @Override
    public void endContact(Contact contact) {
        Object objectA = PhysicsUtils.getObject(contact.getFixtureA().getBody());
        if(objectA != null && objectA instanceof PhysicsObject) {
            ((PhysicsObject)objectA).endContact(contact, true);
        }

        Object objectB = PhysicsUtils.getObject(contact.getFixtureB().getBody());
        if(objectB != null && objectB instanceof PhysicsObject) {
            ((PhysicsObject)objectB).endContact(contact, false);
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        Object objectA = PhysicsUtils.getObject(contact.getFixtureA().getBody());
        if(objectA != null && objectA instanceof PhysicsObject) {
            ((PhysicsObject)objectA).preSolve(contact, oldManifold, true);
        }

        Object objectB = PhysicsUtils.getObject(contact.getFixtureB().getBody());
        if(objectB != null && objectB instanceof PhysicsObject) {
            ((PhysicsObject)objectB).preSolve(contact, oldManifold, false);
        }
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        Object objectA = PhysicsUtils.getObject(contact.getFixtureA().getBody());
        if(objectA != null && objectA instanceof PhysicsObject) {
            ((PhysicsObject)objectA).postSolve(contact, impulse, true);
        }

        Object objectB = PhysicsUtils.getObject(contact.getFixtureB().getBody());
        if(objectB != null && objectB instanceof PhysicsObject) {
            ((PhysicsObject)objectB).postSolve(contact, impulse, false);
        }
    }
}
