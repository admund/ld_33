package me.admund.ld33monster.physics;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import me.admund.framework.draw.animations.AnimationUtils;
import me.admund.framework.draw.holders.AnimationTiledSpriteHolder;
import me.admund.framework.physics.PhysicsUtils;
import me.admund.framework.physics.objects.PhysicsRect;
import me.admund.ld33monster.AssetsList;

/**
 * Created by admund on 2015-08-22.
 */
public class Lava extends PhysicsRect {

    @Override
    public void init(float x, float y, float width, float height) {
        super.init(x, y, width, height);
        AnimationTiledSpriteHolder tiledHolder = new AnimationTiledSpriteHolder(
                AnimationUtils.createAnimation(.25f, AssetsList.LAVA, Animation.PlayMode.LOOP));
        tiledHolder.setObjectSize(width, height);
        setSpriteHolder(tiledHolder, false);
    }

    @Override
    public FixtureDef getFixtureDef() {
        FixtureDef def = new FixtureDef();
        def.shape = PhysicsUtils.getDefaultPolygonShape();
        def.filter.categoryBits = CollisionFilters.CATEGORY_BORDERS;
        def.filter.maskBits = CollisionFilters.MASK_BORDERS;
        return def;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }
}
