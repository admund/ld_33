package me.admund.ld33monster.physics;

import com.badlogic.gdx.graphics.g2d.Batch;
import me.admund.framework.draw.DrawObject;
import me.admund.framework.draw.holders.TiledSpriteHolder;

/**
 * Created by admund on 2015-08-22.
 */
public class Ground extends DrawObject {

    public Ground(String tileName) {
        setSpriteHolder(new TiledSpriteHolder(tileName), false);
    }

    public void init(float posX, float posY, float width, float height) {
        setPosition(posX, posY);
        setSize(width, height);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }
}
