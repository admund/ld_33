package me.admund.ld33monster.physics;

import me.admund.framework.physics.PhysicsObject;
import me.admund.framework.physics.ReuseFactory;
import me.admund.ld33monster.enemies.Enemy;
import me.admund.ld33monster.enemies.PatrolLeaderEnemy;
import me.admund.ld33monster.enemies.PatrolMemeberEnemy;
import me.admund.ld33monster.player.Bottle;
import me.admund.ld33monster.player.Player;
import me.admund.ld33monster.player.Shout;

/**
 * Created by admund on 2015-08-22.
 */
public class MonsterReuseFactory extends ReuseFactory {

    @Override
    public PhysicsObject createNewObj(String className) {
        PhysicsObject obj = null;

        // ENEMIES
        if(className.equals(Enemy.class.toString())) {
            obj = new Enemy();
        } else if(className.equals(PatrolLeaderEnemy.class.toString())) {
            obj = new PatrolLeaderEnemy();
        } else if(className.equals(PatrolMemeberEnemy.class.toString())) {
            obj = new PatrolMemeberEnemy();

        } else if(className.equals(Player.class.toString())) {
            obj = new Player();
        } else if(className.equals(Bottle.class.toString())) {
            obj = new Bottle();

        } else if(className.equals(Lava.class.toString())) {
            obj = new Lava();
        } else if(className.equals(Shout.class.toString())) {
            obj = new Shout();
        }

        return obj;
    }
}
