package me.admund.ld33monster.physics;

/**
 * Created by admund on 2015-08-22.
 */
public class CollisionFilters {
    //public static final short DEFAULT = 0x0001;               // 0000000000000001 in binary
    public static final short CATEGORY_PLAYER =     0x0002;     // 0000000000000010 in binary
    public static final short CATEGORY_ENEMIES =    0x0004;     // 0000000000000100 in binary
    public static final short CATEGORY_SHOUT =      0x0008;     // 0000000000001000 in binary
    public static final short CATEGORY_BORDERS =    0x0010;     // 0000000000010000 in binary
    public static final short CATEGORY_BOTTLE =    0x0020;     // 0000000000100000 in binary

    public static final short MASK_PLAYER = CATEGORY_ENEMIES | CATEGORY_BORDERS | CATEGORY_BOTTLE;
    public static final short MASK_ENEMIES = CATEGORY_BORDERS | CATEGORY_SHOUT | CATEGORY_ENEMIES;
    public static final short MASK_SHOUT = CATEGORY_ENEMIES;
    public static final short MASK_BORDERS = CATEGORY_ENEMIES | CATEGORY_PLAYER;
    public static final short MASK_BOTTLE = CATEGORY_PLAYER;
}
