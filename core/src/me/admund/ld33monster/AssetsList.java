package me.admund.ld33monster;

/**
 * Created by admund on 2015-08-22.
 */
public class AssetsList {
    public final static String basicFont = "fonts/font.fnt";
    public final static String mainTheme = "music/main_theme.ogg";
    public final static String shout = "music/shout.ogg";
    public final static String screm = "music/screm.ogg";

    public static final String MAIN_ATLAS_NAME = "all.atlas";

    public static final String PLAYER_MOVE_RIGHT = "player_move_right";
    public static final String PLAYER_MOVE_LEFT = "player_move_left";
    public static final String PLAYER_IDLE = "player_idle";

    public static final String ENEMY_G_MOVE_RIGHT = "baby_g_move_right";
    public static final String ENEMY_G_MOVE_LEFT = "baby_g_move_left";
    public static final String ENEMY_G_FALL = "baby_g_fall";
    public static final String ENEMY_B_MOVE_RIGHT = "baby_b_move_right";
    public static final String ENEMY_B_MOVE_LEFT = "baby_b_move_left";
    public static final String ENEMY_B_FALL = "baby_b_fall";

    public static final String LAVA = "lava";
    public static final String BOTTLE = "bottle";

    public static final String GROUND = "ground";
    public static final String CLIFF_UP = "cliff_up";
    public static final String CLIFF_DOWN = "cliff_down";

    public static final String WAVE = "wave";
}
