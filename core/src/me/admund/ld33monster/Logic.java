package me.admund.ld33monster;

import com.badlogic.gdx.scenes.scene2d.Group;
import me.admund.framework.achievements.IAchievementsProvider;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.ld33monster.enemies.EnemiesList;
import me.admund.ld33monster.player.Player;

/**
 * Created by admund on 2015-08-22.
 */
public class Logic {
    public static Player player = null;
    public static EnemiesList enemiesList = null;
    public static Group container = null;
    public static PhysicsWorld world = null;

    public static String playerName = "";
    public static IAchievementsProvider achievementsProvider = null;
}
