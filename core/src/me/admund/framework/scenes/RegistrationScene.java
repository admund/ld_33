package me.admund.framework.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import me.admund.framework.GameUtils;
import me.admund.ld33monster.AssetsList;
import me.admund.ld33monster.GameScene;
import me.admund.ld33monster.Logic;

/**
 * Created by admund on 2015-08-22.
 */
public class RegistrationScene extends AbstractScene {
    private float screenWidth = Gdx.graphics.getWidth();
    private float screenHeight = Gdx.graphics.getHeight();

    @Override
    public void create() {
        Gdx.input.setInputProcessor(new ResistrationInputAdapter());
        guiStage.addActor(new Registration());
    }

    class Registration extends Actor {
        public void draw(Batch batch, float parentAlpha) {

            BitmapFont font = GameUtils.assetsManager.getBitmapFont(AssetsList.basicFont);
            font.setColor(Color.WHITE);

            font.draw(batch, "INSTRUCTION:", screenWidth / 4, screenHeight * .9f);
            font.draw(batch, "Move: ARROWS or WSAD", screenWidth / 4, screenHeight * .8f);
            font.draw(batch, "Shout: mouse Click", screenWidth / 4, screenHeight * .7f);

            font.getData().setScale(2);
            font.draw(batch, "Type your name and press ENTER", screenWidth / 4, screenHeight / 2);

            font.getData().setScale(1);
            font.draw(batch,
                    Logic.playerName.length() == 0 ? "PLEASE TYPE SOMETHING :)" : "Your name: " +Logic.playerName,
                    screenWidth / 4, screenHeight / 4);
        }
    }

    class ResistrationInputAdapter extends InputAdapter {
        @Override
        public boolean keyDown(int keycode) {
            if(keycode == Input.Keys.BACKSPACE) {
                removeOneCharacter();
                return true;
            } else if(keycode == Input.Keys.ENTER) {
                if(Logic.playerName.length() > 0) {
                    ScenesManager.inst().push(new GameScene(), true);
                }
                return true;
            }
            return false;
        }

        @Override
        public boolean keyTyped(char character) {
            if (Character.isLetter(character)) {
                Logic.playerName += character;
                return true;
            }
            return false;
        }

        private void removeOneCharacter() {
            if(Logic.playerName.length() > 0) {
                Logic.playerName = Logic.playerName.substring(0, Logic.playerName.length()-1);
            }
        }
    }

}
