package me.admund.framework.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import me.admund.framework.GameConfig;
import me.admund.ld33monster.MonsterGame;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(GameConfig.GAME_WIDTH, GameConfig.GAME_HEIGHT);
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return new MonsterGame(new HtmlAchivmentsProvider());
        }
}